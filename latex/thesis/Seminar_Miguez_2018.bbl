\begin{thebibliography}{1}

\bibitem{Keyes2012}
David~E. Keyes, Lois~C. McInnes, Carol Woodward, and William Gropp.
\newblock {\em Multiphysics simulations: Challenges and opportunities}.
\newblock The International Journal of High Performance Computing Applications,
  2012.

\bibitem{Jaure2013}
S.~Jaure, F.~Duchaine, G.~Staffelbach, and L.~Gicquel1.
\newblock {\em Massively parallel conjugate heat transfer methods relying on
  large eddy simulation applied to an aeronautical combustor}.
\newblock Computational Science and Discovery 6, 2013.

\bibitem{Dawes2010}
W.~Dawes, W.~Kellar, and S.~Harvey.
\newblock {\em Generation of conjugate meshes for complex geometries for
  coupled multi-physics simulations}.
\newblock AIAA, 2010-162.

\bibitem{Schlottke-Lakemper2016}
Michael Schlottke-Lakemper, Matthias Meinke, and Wolfgang Schr{\"o}der.
\newblock {\em A Hybrid Discontinuous Galerkin-Finite Volume Method for
  Computational Aeroacoustics}, pages 743--753.
\newblock Springer International Publishing, 2016.

\bibitem{Schlottke-Lakemper2017}
Michael Schlottke-Lakemper, Yu~Hans, Sven Berger, Matthias Meinke, and Wolfgang
  Schr{\"o}der.
\newblock {\em A fully coupled hybrid computational aeroacoustics method on
  hierarchical Cartesian meshes}, pages 137--153.
\newblock Computers and Fluids.

\bibitem{Schlottke-Lakemper2018}
Michael Schlottke-Lakemper, Ansgar Niem{\"o}ller, Matthias Meinke, and Wolfgang
  Schr{\"o}der.
\newblock {\em Efficient parallelization for volume-coupled multiphysics
  simulations}.
\newblock CMAME, 2018.

\bibitem{Lintermann2014}
A.~Lintermann, S.~Schlimpert, J.~H. Grimmen, C.~G{\"u}nther, Matthias Meinke,
  and Wolfgang Schr{\"o}der.
\newblock {\em Massively parallel grid generation on HPC systems}, pages
  131--153.
\newblock Computer Methods in Applied Mechanics and Engineering, 2014.

\bibitem{Muller1967}
F.~M{\"u}ller, E. A.~Obermeier.
\newblock The spinning vortices as a source of sound.
\newblock In {\em AGARD Conference Proceedings}, volume~22, 1967.

\end{thebibliography}
