 % !TeX spellcheck = en_GB
\chapter{The direct-hybrid method}
\label{sec:The direct-hybrid method}

When dealing with a classical hybrid method, the flow and aeroacoustics solvers run sequentially so
the source terms are transferred on disk from the CFD cells to the CAA cells. Moreover, in the
cases where the CFD and the CAA have different grid topologies, an interpolation step is necessary
to transfer the data from one grid to another. In the direct-hybrid method, however, the solvers
run concurrently in a joint hierarchical Cartesian grid. This assures a more efficient data
transfer and the possibility to exchange data in-memory between the solvers.

\section{Topology of the hierarchical Cartesian mesh}

The grid generation of the hierarchical Cartesian grid begins with a single cubic cell block that
encloses the entire domain \cite{Lintermann2014}. This single cell block is then subjected to a
successively refinement, where the original cell is isotropically refined into eight cubic cells,
until the minimum cell refinement is obtained (Fig.\ref{fig:cell-refinement}). Additionally,
specific regions of the grid can be further refined to meet numerical or physical requirements
\cite{Schlottke-Lakemper2016}.\\
Figures \ref{fig:aeroacoustics1} and \ref{fig:aeroacoustics2} depicts an example of a 2D grid with
four refinement levels that can be applied to an aeroacoustics simulation. In the figure, red
represents CFD cells, beige represents the CAA cells, and orange represent cells share by both the
CFD and CAA domains. As one can observe, the CAA domain has cells of ${l+1}$ and ${l+2}$ refinement
level with the higher refinement at the center of the domain. In contrast, the CFD has cells of all
refinement levels with also a higher refinement level at the center. This shows that the individual
solvers have independent grid requirements due to the governing equations being solved in each
individual grid.

\begin{figure}[ht]
	\centering
	\includegraphics[scale=0.27]{figures/fig_1.png}
	\captionof{figure}{Cell refinement of a 2D grid.}
	\label{fig:cell-refinement}
\end{figure}
\vspace{1cm}
\begin{figure}[ht]
	\centering
	\begin{subfigure}[b]{.43\textwidth}
		\raggedright
		\includegraphics[width=\linewidth]{figures/fig_2a.pdf}
		\captionof{figure}{Exploded view.}
		\label{fig:aeroacoustics1}
	\end{subfigure}%
\hspace{0.8cm}
	\begin{subfigure}[b]{.23\textwidth}
		\raggedleft
		\includegraphics[width=\linewidth]{figures/fig_2b.pdf}
		\captionof{figure}{Top view.}
		\label{fig:aeroacoustics2}
	\end{subfigure}
	\caption{Example of a 2D joint Cartesian grid \cite{Schlottke-Lakemper2017}.}
\end{figure}

\section{Domain decomposition}

In order to achieve an efficient communication, the cells that exchange information must be
assigned to the same MPI subdomain. For that, the grid is partitioned using a Hilbert space-filling
curve. This assures that the cells of a subtree are designated to the same MPI rank, so the data to
be exchanged is available in-memory. The load balancing is then reached by pre-measuring the
workload of each solver separately so the cells of the subtrees are assigned to distinct
computational weights. At this point the workload can be uniformly distributed among the
processors. Figure \ref{fig:tree-decomposition} is a quadtree representation of two subtrees with
a mixture of pure CFD, pure CAA, and mixed CFD and CAA cells.\\

\begin{figure}[h]
	\centering
	\includegraphics[scale=1.2]{figures/fig_3.pdf}
	\captionof{figure}{Domain decomposition \cite{Schlottke-Lakemper2017}.}
	\label{fig:tree-decomposition}
\end{figure}

\section{Parallel Algorithm}

For our multiphysics coupling we assume that the solvers only communicate once every time step,
i.e., a solver can only begin the next time step after the other solver has finished the previous
step. Additionally, each solver time step is divided into substeps which require an intra-solver
communication between consecutive substeps. Consequently, the next substep can only begin after
the previous substep has finished in all subdomains. \\
Figure \ref{fig:para-idle} shows a simple parallel algorithm that continuously advances each solver
one time step. This generates large idle times due to the presence of more than one solver in the
same subdomain since the next substep is unable to start until the previous finishes in all
subdomains. Therefore, the domain with higher single solver workload act as the bottle neck.\\
A way to circumvent this problem is to alternate the execution of the solvers substeps during a
time step execution. Moreover, it is also possible to overlap communication and computation of the
different solvers subdomains with both CFD and CAA cells. Figure \ref{fig:para-performance}
presents the time line of this parallel algorithm for different substep stages.\\

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.25]{figures/fig_4.png}
	\captionof{figure}{Simple parallel algorithm \cite{Schlottke-Lakemper2018}.}
	\label{fig:para-idle}
\end{figure}

\vspace{10cm}

\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.25]{figures/fig_5.png}
	\captionof{figure}{Efficient parallel algorithm \cite{Schlottke-Lakemper2018}.}
	\label{fig:para-performance}
	\newpage
\end{figure}
