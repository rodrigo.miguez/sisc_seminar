\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}The direct-hybrid method}{2}{chapter.2}
\contentsline {section}{\numberline {2.1}Topology of the hierarchical Cartesian mesh}{2}{section.2.1}
\contentsline {section}{\numberline {2.2}Domain decomposition}{3}{section.2.2}
\contentsline {section}{\numberline {2.3}Parallel Algorithm}{4}{section.2.3}
\contentsline {chapter}{\numberline {3}Numerical Methods}{6}{chapter.3}
\contentsline {section}{\numberline {3.1}Flow simulation}{6}{section.3.1}
\contentsline {section}{\numberline {3.2}Acoustics simulation}{7}{section.3.2}
\contentsline {section}{\numberline {3.3}Spatial interpolation}{7}{section.3.3}
\contentsline {chapter}{\numberline {4}Numerical Results}{9}{chapter.4}
\contentsline {section}{\numberline {4.1}Spinning vortex pair setup}{9}{section.4.1}
\contentsline {section}{\numberline {4.2}Spinning vortex pair results}{11}{section.4.2}
\contentsline {section}{\numberline {4.3}Parallel performance results}{12}{section.4.3}
\contentsline {chapter}{\numberline {5}Conclusions}{14}{chapter.5}
\contentsline {chapter}{\numberline {6}Bibliography}{15}{chapter.6}
