 % !TeX spellcheck = en_GB
\chapter{Numerical Methods}
\label{sec:Numerical Methods}

As stated previously, two independent solvers are used in this work to evaluate the direct-hybrid
method. For the flow field computation, an unsteady compressible Navier-Stokes equations solver
based on the finite volume formulation is used while the acoustic perturbation equations (APE) are
used to obtain the acoustic field.\\

\section{Flow simulation}

The conservative and non-dimensional compressible Navier-Stokes equations read as follows

\begin{align}
\frac{\partial \rho}{\partial t} +
{\boldsymbol{\nabla} \cdot ( \rho \boldsymbol{u} )} = 0
\end{align}

\begin{align}
\frac{\partial \rho \boldsymbol{u}}{\partial t} +
{\boldsymbol{\nabla} \cdot \Big( \rho \boldsymbol{u} \boldsymbol{u} + p \boldsymbol{I} +
 \frac{\tau}{Re_0} \Big)} = 0
\end{align}

\begin{align}
\frac{\partial \rho E}{\partial t} +
{\boldsymbol{\nabla} \cdot \Big( (\rho E + p) \boldsymbol{u} +
 \frac{1}{Re_0}(r \boldsymbol{u} + \boldsymbol{q}) \Big)} = 0
\end{align}

where ${\rho}$ is the density of the fluid, $\boldsymbol{u}$ is the velocity vector, ${E}$ is the
total specific energy, $\boldsymbol{I}$ is the identity matrix, ${Re_0}$ is the Reynolds number at
the stagnation state, $\boldsymbol{\tau}$ is the stress tensor, and $\boldsymbol{q}$ is the heat
conduction vector. To close the system, the definition of the total specific energy for a perfect
gas is used. Moreover, the stress tensor is assumed as a Newtonian fluid. \\

\section{Acoustics simulation}

For the acoustic simulation the APE equations are used, which are derived from the linearized
Navier-Stokes equations and only possess the acoustic modes. To discretize the APE using a
discontinuous Galerkin method, the APE-4 system in non-dimensional conservative form is used and
it reads as follows

\begin{align}
\frac{\partial \boldsymbol{u'}}{\partial t} +
{\boldsymbol{\nabla}  \Big( \boldsymbol{\overline{u}} \cdot \boldsymbol{u'} +
 \frac{p'}{\overline{p}} \Big)} = \boldsymbol{q}_m
\end{align}

\begin{align}
\frac{\partial p'}{\partial t} +
{\boldsymbol{\nabla} \cdot \big(\overline{c}^2 \overline{\rho} \boldsymbol{u'} +
 \boldsymbol{\overline{u}} p'\big)} - \Big( \overline{\rho} \boldsymbol{u'} +
 \boldsymbol{\overline{u}} \frac{p'}{\overline{c}^2} \Big) \cdot {\nabla \overline{c}^2}  = 0
\end{align}

where the prime denote the fluctuation terms, the bar are the time averaged properties determined
from the flow solution, and ${\boldsymbol{q}_m}$ is the source term. The unknowns of the system are
the fluctuation quantities and due to the discontinuous Galerkin formulation the fluxes are
determined for the Legendre-Gauss nodes at the element faces.\\

\section{Spatial interpolation}

As mentioned previously, the source terms have to be transferred from the CFD to the CAA grid. For
cases where there is relationship of one CFD to one CAA cell and one CFD to many CAA cells the
spatial interpolation is simple, a single source term in the fluid dynamic domain is calculated for
each variable and then transferred to the acoustic domain. However, for many CFD cells to one CAA
cell the mapping is not simple as the first cases due to the piecewise constant representation of
the CFD grid and the high-order polynomial representation of the CAA grid. This later situation is
common when using high-order polynomials in the discontinuous Galerkin discretization where the
number of Gauss nodes are increased and the grid refinement
can be reduced \cite{Schlottke-Lakemper2018}.\\

The spatial interpolation is performed by a local Galerkin projection where the target interpolant
${q^T}$ given by

\begin{align}
{\hat{q}^T}_{ijk} = \sum_{m=1}^{S} {\hat{q}^{D,m}} {\hat{W}^{D,m}}_{ijk}, \hspace{1cm}i,j,k=0,...,N
\end{align}

where ${q^D}$ is the interpolant of the donor cell, ${S}$ is the total number of donor cells, and
${\hat{W}^{D}}$ is the modified the donor cell weight, which represents the the contribution of the
${m}$-th donor cell to the ${ijk}$-th Legendre-Gauss node. Therefore, each element is considered
individually. Moreover, the modified donor cell weights can be precomputed, which is beneficial in
terms of efficiency. For more information regarding the spatial interpolation method, the reader
may refer to \cite{Schlottke-Lakemper2017}.\\
